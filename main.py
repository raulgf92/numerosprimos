#!/usr/bin/env python
# -*- coding: utf-8 -*-

#Números primos

def es_primo(num):
	if num < 2:
		return False
	for i in range(2, num): 
		if num % i == 0:
			return False
	return True   
def primos(num1, num2):  
	cont = 0
  
	for i in range(num1, num2):
		if es_primo(i) == True: #Llamamos a la primera funcion para ahorrar trabajo ;)
			cont += 1           #Que va a determinar si es primo o no
			print i               
  
	print ""  
	print "Hay", cont, "numeros primos" #Total de numeros primos
       
print primos(487, 8976)
